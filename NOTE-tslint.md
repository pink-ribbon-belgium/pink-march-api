# tslint rules

In order to set the same linting rules as in pink-march-ui-immutable we currently use Angular-tslint-rules.
For a library created with node it's not recommended to use this rule set, but to stay consistent with the code structure and rules, we use it anyway.

Angular-tslint-rules contains a set of TSlint and Codelyzer rules. Because Codelyzer only works in Angular projects we lack a set of rules that Codelyzer uses.
These rules are currently manually set to "false".
