# Testing TypeScript Libraries

This library is written in TypeScript. We want to write tests
in TypeScript too, using Jasmine, and execute them while determining code coverage.

TypeScript code cannot be tested directly, because it cannot be executed
directly. The TypeScript code first must be compiled to JavaScript before it can
be executed, and thus tested.

## Compile for test, test, compile for publication

A first approach to test TypeScript code is to first compile the test files (`> tsc -p tsconfig.spec.json`), and then
run the tests with coverage (`> nyc jasmine`). Because `tsconfig.spec.json` points to the test files, and the test files
import the `src/` files, the `src/` files get compiled too, and this compilation result is executed. In the `"test"`
command below, we first erase the previous compilation result, lint and generate documentation, to make sure we do not
get false results, that the formatting is ok and there are no anti-patterns, and that the `tsdoc` annotations are ok.

When we want to publish the library, we first `"build"` the result (`> tsc -p tsconfig.app.json`) to `dist/dto`,
and then package that directory.

    {
      […]
      "scripts": {
        […]
        "test": "rm -Rf dist/test && npm run lint && npm run doc && tsc -p tsconfig.spec.json && nyc jasmine",
        "build": "rm -Rf dist/dto && tsc -p tsconfig.app.json",
        […]
      }
      […]
    }

This approach is weird, because we actually package another artifact (`dist/dto`) then is tested (`dist/test/src`).
We could get away with this, because we might assume that the TypeScript compiler behaves deterministic, and produces
the same result in both cases. However, we use a different `tsconfig` in both cases, so this might introduce
differences by accident, and should not be considered safe.

## Test with ts-node

Most articles on the web describe a different approach, using [`ts-node`] ([Writing unit tests in TypeScript],
[TypeScript Unit Testing with Test Coverage],[Unit testing a TypeScript library],
[Writing a library with TypeScript and proper test coverage],
[Unit testing node applications with TypeScript — using mocha and chai], …). [`ts-node`], in short, makes Node
understand TypeScript. The runtime will compile TypeScript _on-the-fly_. [mocha] used to have internalized support for
this with the `--compiler` CLI option, which was since removed, but explains how you can use
`--require ts-node/register` just as good. [`jasmine-ts`] makes this a bit easier for Jasmine.

However, this approach suffers from the same problems as the first. Now, we do not create separate compiled code for the
tests on disk, but we do it on-the-fly. Same difference.

The examples above do not introduce a separate `tsconfig.spec.json`. In our setup, the only use for a separate
`tsconfig.app.json` and `tsconfig.spec.json` is to _not_ mention Jasmine types in `tsconfig.app.json`. In this approach,
that is not necessary, because Jasmine is loaded in `ts-node`. However, this will impact code completion in the IDE,
and linting in the IDE and CLI.

## Compile for publication, write tests against compiled artifact, compile, and test

In this approach, we first build the artifact (after linting and checking documentation generation), from `src/` to
`lib/` with `tsconfig.app.json`. The tests we write in `spec` refer to the `.js` / `.d.ts` compiled artifact in `lib/`,
_not_ directly to `src/`. The `spec/` code is compiled to with `tsconfig.spec.json` to `test/`, and executed there.

The negative point here is that, when `scr/` code changes, we need to `"build"` again to get the compiled version. This
is only an issue in the IDE. To avoid this, the IDE is set up with a separate `jasmine-IDE.json` file, that refers to
`spec/`, instead of `test/`. The IDE compiles all files in place, so these are ignored in `.gitignore`.

[`ts-node`]: https://www.npmjs.com/package/ts-node
[writing unit tests in typescript]: https://medium.com/@RupaniChirag/writing-unit-tests-in-typescript-d4719b8a0a40
[typescript unit testing with test coverage]: https://medium.com/swlh/typescript-unit-testing-with-test-coverage-2cc0cc6f3fd1
[unit testing a typescript library]: https://www.tsmean.com/articles/how-to-write-a-typescript-library/unit-testing/
[writing a library with typescript and proper test coverage]: https://ole.michelsen.dk/blog/writing-library-with-typescript-test-coverage.html
[unit testing node applications with typescript — using mocha and chai]: https://journal.artfuldev.com/unit-testing-node-applications-with-typescript-using-mocha-and-chai-384ef05f32b2
[mocha]: https://github.com/mochajs/mocha/wiki/compilers-deprecation
[`jasmine-ts`]: https://www.npmjs.com/package/jasmine-ts
