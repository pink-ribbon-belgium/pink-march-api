# pink-march-api

Definition of the structure of DTOs used in communication with the back-end.

This starts out during Analysis, Architecture and Design with the definitions of interface and types.

Since DTOs are needed both in the SPA clients and server, and the transport format is JSON, the types are defined
in TypeScript in AAD.

The types might be different for reading from, and writing to, the back-end, since more information is returned from the
backend, then are required for writing.

The types are defined with [Joi], and are specified in TypeScript.

## Target environment

This application is developed for the latest version of Node 12.

# Development

## Build first

Tests (`spec/`) are written referring to the build artifact `.js` / `.d.ts` files in `lib/`, _not_ referring directly to
the `src/` files. To get this working in the IDE, run `npm run build:lib` first, to generate `lib/`. Or better yet,
do `npm test` to generate everything and run the tests.

See [Testing TypeScript Libraries] for details.

## IDE

JetBrains WebStorm settings are included and kept up-to-date by all developers.

## Publish

To publish

- Make sure everything is code for the feature
- Decide whether this is a major, minor, or patch evolution
- Merge to master, and check CI is ok
- Do, on master

      > npm version [major | minor | patch]

  This changes `package.json`, `package-lock.json`, and tags the `git` repository.

- Push
- Do

      > npm publish

[joi]: https://hapi.dev/family/joi/?v=16.1.7
[testing typescript libraries]: TestingTS.md
[authenticate with registry]: scripts/common/npm/authenticateWithRegistry.md#how-to-authenticate-as-developer
